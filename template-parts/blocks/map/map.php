<section class="section section-map">
  <div class="container">
    <h2 class="section-title section-title--map">
      <span><?php echo get_field('section_map_title_black')?></span>
      <?php echo get_field('section_map_title_blue')?>
    </h2>
    <p class="section-subtitle"><?php echo get_field('section_map_subtitle')?></p>
    <iframe class="section section-map__iframe" frameborder="0" src="<?php echo get_field('section_map_url')?>"></iframe>
    <div class="section-map__content">
      <h2 class="section-title section-title--map-content">
        <span><?php echo get_field('section_map_bottom_title_black')?></span>
        <?php echo get_field('section_map_bottom_title_blue')?>
      </h2>
      <p class="section-map__descr"><?php echo get_field('section_map_bottom_description')?></p>
    </div>
  </div>
</section>