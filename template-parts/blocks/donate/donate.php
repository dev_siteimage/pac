<section class="section section-donate">
  <div class="container">
    <div class="section-donate__wrapper">
      <div class="section-donate__image">
        <img src="<?php echo get_field('section_donate_image')?>" alt="<?php echo get_field('section_donate_button')?>">
      </div>
      <div class="section-donate__content">
        <h2 class="section-title section-title--donate">
          <span><?php echo get_field('section_donate_title_yellow')?></span>
          <?php echo get_field('section_donate_title_black')?>
          <span><?php echo get_field('section_donate_title_blue')?></span>
        </h2>
        <p class="section-donate__text"><?php echo get_field('section_donate_text')?></p>
        <a href="#" class="btn"><?php echo get_field('section_donate_button')?></a>
      </div>
    </div>
  </div>
</section>