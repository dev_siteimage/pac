<section class="section section-video">
<div class="container">
  <div class="section-video__wrapper">
  <div class="section-video__content">
    <h1 class="section-video__title"><?php echo get_field('main_video_title')?></h1>
    <strong class="section-video__subtitle"><?php echo get_field('main_video_subtitle')?></strong>
    <p class="section-video__text"><?php echo get_field('main_video_text')?></p>
    <a href="#" class="btn"><?php echo get_field('main_video_button_text')?></a>
    <p class="section-video__descr"><span><?php echo get_field('main_video_description_blue')?></span><?php echo get_field('main_video_description')?></p>
  </div>
  <div class="section-video__video " data-video-container>
    <video autoplay="" muted="" loop="" playsinline="" aria-label="Video" data-video>
      <source src="<?php bloginfo('template_directory'); ?>/resources/images/video.mp4" type="video/mp4">
    </video>
    <button class="btn-video play" data-video-button>
      <span></span>
      <span></span>
    </button>
  </div>
  </div>
</div>
</section>