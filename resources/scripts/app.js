/**
 * External Dependencies
 */
import 'jquery';

$(document).ready(() => {

  const header = document.querySelector(".header");
    const headerScrollUp = "header--up";
    const headerScrollDown = "header--down";
    let lastScroll = 0;

    window.addEventListener("scroll", () => {
        const currentScroll = window.pageYOffset;
        if (currentScroll <= 0) {
            header.classList.remove(headerScrollUp);
            return;
        }

        if (currentScroll > lastScroll && !header.classList.contains(headerScrollDown)) {
            // down
            header.classList.remove(headerScrollUp);
            header.classList.add(headerScrollDown);
        } else if (currentScroll < lastScroll && header.classList.contains(headerScrollDown)) {
            // up
            header.classList.remove(headerScrollDown);
            header.classList.add(headerScrollUp);
        }
        lastScroll = currentScroll;
    });

    $(document).on('click', '[data-menu-button]', function() {
      let th = $(this);
      let menu = $('.nav-primary')
      $('body').toggleClass('fixed');
      th.toggleClass('active');
      menu.toggleClass('active');
  });


  $(window).on('load', function() {
    if ($(".js-main-menu li").find(".sub-menu").length > 0){ 
      $(this).addClass('active')
    }
  })

  $(document).on('click', '[data-video-button]', function() {
    var th = $(this);
    var videoContainer = th.closest('[data-video-container]');
    var video = videoContainer.find('[data-video]');
    if (th.hasClass('play')){
      th.removeClass('play');
      th.addClass('paused');
      video.trigger('pause');
    } else if(th.hasClass('paused')) {
      th.addClass('play');
      th.removeClass('paused');
      video.trigger('play');
    }
  })
});
