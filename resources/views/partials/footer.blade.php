<footer class="footer">
  <div class="container">
    <div class="footer__content">
      @php(dynamic_sidebar('sidebar-footer'))
    </div>
    <div class="footer__bottom">
      <p class="footer__bottom-text">The information provided on Prevention Access Campaign is intended to support rather than replace consultation with a healthcare professional. Always consult a healthcare professional for medical advice, diagnosis, and treatment about your particular situation. </p>
      <p class="footer__bottom-copy">Prevention Access Campaign is fiscally sponsored by Social & Environmental Entrepreneurs, a 501 (c) 3 public charity, and formerly by Housing Works.</p>
    </div>


  </div>
</footer>
