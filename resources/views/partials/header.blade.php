<header class="header">
  <div class="header__top">
      <p class="header__top-text">{!! __('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.', 'sage') !!}</p>
      <a href="#" class="header__top-link">{!! __('Click here', 'sage') !!}</a>
  </div>
  <div class="header__content">
      <a class="logo" href="{{ home_url('/') }}">
        <img class="logo__img" src="<?php bloginfo('template_directory'); ?>/resources/images/logo.png" alt="Logo Customer">
      </a>
    
      <nav class="nav-primary">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'main-menu js-main-menu', 'echo' => false]) !!}
        @endif
      </nav>
    
      <button type="button" class="btn">Educate & Act</button>

      <div class="hamburger" data-menu-button>
        <span></span>
      </div>

  </div>
</header>
