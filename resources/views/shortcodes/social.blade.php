<?php 

  add_shortcode('social', function() {
    
     $atts = shortcode_atts( array(
         'start' => '',
         'end' => '',
     ), $atts );

    ob_start(); 
    $fields = get_field('social_item', 'option'); ?>
<ul class="social">

    <?php foreach($fields as $field) : 
        if ($field['social_icon']) : ?>

    <li class="social__item">
        <a class="social__link" href="<?php echo $field['social_url']; ?>">
            <span class="social__icon"><?php echo file_get_contents($field['social_icon']); ?></span>
        </a>
    </li>

    <?php endif; endforeach; ?>

</ul>

<?php return ob_get_clean();
  });
