<?php

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our theme. We will simply require it into the script here so that we
| don't have to worry about manually loading any of our classes later on.
|
*/

if (! file_exists($composer = __DIR__ . '/vendor/autoload.php')) {
    wp_die(__('Error locating autoloader. Please run <code>composer install</code>.', 'sage'));
}

require $composer;

/*
|--------------------------------------------------------------------------
| Run The Theme
|--------------------------------------------------------------------------
|
| Once we have the theme booted, we can handle the incoming request using
| the application's HTTP kernel. Then, we will send the response back
| to this client's browser, allowing them to enjoy our application.
|
*/

require_once __DIR__ . '/bootstrap/app.php';

require dirname( __FILE__ ) . '/template-parts/shortcodes/init.php';


if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Social Settings',
		'menu_title'	=> 'Social',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}



function register_acf_block_types (){
	acf_register_block_type(array(
		'name' => 'banner',
		'title' => __('Banner'),
		'description' => 'Custom banner block',
		'render_template' => 'template-parts/blocks/banner/main-banner.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('banner', 'product')
	));
	acf_register_block_type(array(
		'name' => 'map',
		'title' => __('Map'),
		'description' => 'Custom map block',
		'render_template' => 'template-parts/blocks/map/map.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('map', 'product')
	));
	acf_register_block_type(array(
		'name' => 'donate',
		'title' => __('Donate'),
		'description' => 'Custom donate block',
		'render_template' => 'template-parts/blocks/donate/donate.php',
		'icon' => 'editor-paste-text',
		'keywords' => array('donate', 'product')
	));
}


if(function_exists('acf_register_block_type')) {
	add_action('acf/init', 'register_acf_block_types');
}
